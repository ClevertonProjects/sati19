# Description

Sidescroller game tutorial, used on SATI PG 19's unity development workshop.
Download the project and open it on Unity 2019.2+

The game scene and all scripts are inside the "Complete" folder.

# Inputs

* Movement: A/D or LeftArrow/RightArrow
* Jump: space
* Interact: F
* Grab: E
* Throw/Use: Q
