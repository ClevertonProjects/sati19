﻿using UnityEngine;
using UnityEngine.Events;

namespace SATI19Complete
{
    /*! \class TriggerPanels
     *  \brief Controls the interactable panels.
     */
    public class TriggerPanels : MonoBehaviour, IInteractables
    {        
        public PanelRequirements Requirement;                               //!< Set a requirement to trigger this panel.                

        public UnityEvent OnInteractConfirmed;                              //!< Event called on panel interaction.

        /// <summary>
        /// Disables the object linked to this panel.
        /// </summary>
        public void Interact()
        {
            GetComponent<AudioSource>().Play();

            if (Requirement != PanelRequirements.None)
            {
                GameController gameController = GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<GameController>();

                if (gameController.IsRequirementAchieved((int)Requirement))
                {
                    OnInteractConfirmed.Invoke();
                }
            }
            else
            {
                OnInteractConfirmed.Invoke();
            }
        }
    }
}
