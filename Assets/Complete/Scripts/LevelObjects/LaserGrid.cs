﻿using UnityEngine;

namespace SATI19Complete
{
    /*! \class LaserGrid
     *  \brief Controls the laser grid deactivation.
     */
    public class LaserGrid : MonoBehaviour
    {
        public GameObject LaserCollider;                                     //!< Collider blocking the passage.
        public ParticleSystem LaserEffect;                                   //!< Laser effect.

        /// <summary>
        /// Deactivates the laser grid.
        /// </summary>
        public void Deactivate ()
        {
            ParticleSystem.MainModule particleModule = LaserEffect.main;
            particleModule.loop = false;            

            LaserCollider.SetActive(false);
            GetComponent<AudioSource>().Stop();
        }
    }
}
