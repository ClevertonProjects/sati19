﻿using UnityEngine;
using System.Collections;

namespace SATI19Complete
{
    /*! \class Teleporter
     *  \brief Controls the teleport item movement.
     */
    public class Teleporter : MonoBehaviour, ICollectableItems
    {
        public float ThrowForce;                                    //!< Force applied to the object on spawn.
        private bool isReadyToUse;                                  //!< Check if the teleporter is stopped and ready to use.

        private const float MinSpeedToRest = 1f;                    //!< Minimum speed to consider the item is at rest.

        public bool IsReadyToUse
        {
            get { return isReadyToUse; }
        }        

        /// Routine to check when the item is at rest.
        private IEnumerator RestCheckRoutine (Rigidbody rb)
        {
            WaitForSeconds delay = new WaitForSeconds(0.5f);

            do
            {            
                yield return delay;                
            } while (rb.velocity.sqrMagnitude > MinSpeedToRest);

            isReadyToUse = true;
        }

        /// <summary>
        /// Applies an impulse to the object.
        /// </summary>
        public void Throw (Vector3 fromPosition, Quaternion rotation)
        {   
            Rigidbody rb = GetComponent<Rigidbody>();
            Transform myTransform = transform;
            myTransform.position = fromPosition;
            myTransform.rotation = rotation;

            gameObject.SetActive(true);

            rb.AddForce(transform.forward * ThrowForce);
            StartCoroutine(RestCheckRoutine(rb));            
        }        

        /// <summary>
        /// Returns the actual position of the teleporter item.
        /// </summary>        
        public Vector3 GetTeleporterPosition ()
        {
            return GetComponent<Rigidbody>().position;
        }

        /// <summary>
        /// Event called when the player grabs the teleport item.
        /// </summary>
        public void OnCollect()
        {
            isReadyToUse = false;
            gameObject.SetActive(false);
        }
    }
}
