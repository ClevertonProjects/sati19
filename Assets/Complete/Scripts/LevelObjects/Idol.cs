﻿using UnityEngine;

namespace SATI19Complete
{
    /*! \class Idol
     *  \brief Controls the idol collect state.
     */
    public class Idol : MonoBehaviour, ICollectableItems
    {
        public PanelRequirements IdolType;                          //!< Idol requirement type.

        /// <summary>
        /// Sets the idol state as collected in the game controller.
        /// </summary>
        public void OnCollect()
        {
            GameController gameController = GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<GameController>();
            gameController.CollectItem((int)IdolType);

            gameObject.SetActive(false);
        }
    }
}
