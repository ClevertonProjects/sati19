﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace SATI19Complete
{
    /*! \class SpikeTrap
     *  \brief Controls the spike trap movement.
     */
    public class SpikeTrap : MonoBehaviour
    {
        public float MovementInDuration;                                        //!< Time duration of the movement form begin to end point.
        public float MovementOutDuration;                                       //!< Time duration of the movement form end to begin point.
        public Transform StartingPoint;                                         //!< Movement starting position.
        public Transform EndingPoint;                                           //!< Movement ending position.        

        public AnimationCurve MoveOutCurve;                                     //!< Curve defining the timing behavior of the movement out.
        public AnimationCurve MoveInCurve;                                      //!< Curve defining the timing behavior of the movement in.
        
        public bool IsDirectionFlipped;                                         //!< Check to invert the movement direction.

        public AudioSource SpikesMoveSound;                                     //!< Sound effect played when the spikes moves out.

        public UnityEvent OnMovementEnded;                                      //!< Event triggered when the full movement out/in ends.

        private Transform myTransform;                                          //!< Transform reference.

        // Start is called before the first frame update
        void Start()
        {
            myTransform = transform;            
        }

        /// Iterates the object position.
        private IEnumerator MovementLoop(Vector3 from, Vector3 to, float duration)
        {
            WaitForEndOfFrame delay = new WaitForEndOfFrame();
            float elapsedTime = 0f;
            float proportion = 0f;            

            while (proportion <= 1f)
            {
                float t = IsDirectionFlipped ? MoveInCurve.Evaluate(proportion) : MoveOutCurve.Evaluate(proportion);
                myTransform.position = Vector3.Lerp(from, to, t);
                elapsedTime += Time.smoothDeltaTime;
                proportion = elapsedTime / duration;

                yield return delay;
            }

            IsDirectionFlipped = !IsDirectionFlipped;

            if (IsDirectionFlipped)
            {
                StartMovement();
            }
            else
            {
                OnMovementEnded.Invoke();
            }
        }

        /// <summary>
        /// Starts the movement loop.
        /// </summary>
        public void StartMovement()
        {
            Vector3 from;
            Vector3 to;
            float duration;

            if (IsDirectionFlipped)
            {
                from = EndingPoint.position;
                to = StartingPoint.position;
                duration = MovementInDuration;
            }
            else
            {
                if (SpikesMoveSound != null)
                {
                    SpikesMoveSound.Play();
                }

                from = StartingPoint.position;
                to = EndingPoint.position;
                duration = MovementOutDuration;
            }

            StartCoroutine(MovementLoop(from, to, duration));
        }
    }
}
