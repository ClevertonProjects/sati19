﻿using UnityEngine;

namespace SATI19Complete
{
    /*! \class Door
     *  \brief Controls the level doors deactivation.
     */
    public class Door : MonoBehaviour
    {
        public Animator TriggerAnimator;                                    //!< Door's animator.
        public GameObject DoorCollider;                                     //!< Collider blocking the passage.

        private readonly int hashAct = Animator.StringToHash("Act");        //!< Hash of the parameter that triggers the animation on act.

        /// <summary>
        /// Deactivates the door.
        /// </summary>
        public void Deactivate ()
        {
            GetComponent<AudioSource>().Play();
            TriggerAnimator.SetTrigger(hashAct);
            DoorCollider.SetActive(false);
        }
    }
}
