﻿using UnityEngine;

namespace SATI19Complete
{
    /*! \class DoubleJumpBoot
     *  \brief Controls the double jump boot collect state.
     */
    public class DoubleJumpBoot : MonoBehaviour, ICollectableItems
    {
        /// <summary>
        /// Activates the player double jump hability.
        /// </summary>
        public void OnCollect()
        {
            PlayerController player = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
            player.UnlockDoubleJump();

            gameObject.SetActive(false);
        }
    }
}
