﻿using UnityEngine;
using System.Collections;

namespace SATI19Complete
{
    /*! \class MovableTrap
     *  \brief Controls the movement of the trap.
     */
    public class MovableTrap : MonoBehaviour
    {
        public float MovementDuration;                                          //!< Time duration of the movement form begin to end point.
        public Transform StartingPoint;                                         //!< Movement starting position.
        public Transform EndingPoint;                                           //!< Movement ending position.

        public bool IsDirectionFlipped;                                         //!< Check to invert the movement direction.

        private Transform myTransform;                                          //!< Transform reference.

        // Start is called before the first frame update
        void Start()
        {
            myTransform = transform;
            StartMovement();
        }

        /// Iterates the object position.
        private IEnumerator MovementLoop (Vector3 from, Vector3 to, float duration)
        {
            WaitForEndOfFrame delay = new WaitForEndOfFrame();
            float elapsedTime = 0f;
            float proportion = 0f;

            while (proportion <= 1f)
            {
                myTransform.position = Vector3.Lerp(from, to, proportion);
                elapsedTime += Time.smoothDeltaTime;
                proportion = elapsedTime / duration;

                yield return delay;
            }

            IsDirectionFlipped = !IsDirectionFlipped;
            StartMovement();
        }

        /// Starts the movement loop.
        private void StartMovement ()
        {
            Vector3 from;
            Vector3 to;

            if (IsDirectionFlipped)
            {
                from = EndingPoint.position;
                to = StartingPoint.position;
            }
            else
            {
                from = StartingPoint.position;
                to = EndingPoint.position;
            }

            StartCoroutine(MovementLoop(from, to, MovementDuration));
        }        
    }
}
