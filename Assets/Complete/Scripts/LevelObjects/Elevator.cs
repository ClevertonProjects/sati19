﻿using UnityEngine;

namespace SATI19Complete
{
    /*! \class Elevator
     *  \brief Controls the level elevator.
     */
    public class Elevator : MonoBehaviour, IInteractables
    {
        public Vector3 DestinationPoint;                            //!< Position linked with this elevator.

        private const float TeleportDelay = 1f;                     //!< Time to wait before relocating the player.

        /// <summary>
        /// Event called when the character interacts with this object.
        /// </summary>
        public void Interact()
        {
            PlayerController player = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
            player.TriggerRelocation(DestinationPoint, TeleportDelay);
        }        
    }
}
