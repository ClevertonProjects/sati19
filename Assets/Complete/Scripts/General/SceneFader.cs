using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace SATI19Complete
{
    /*! \class SceneFader
     *  \brief Controls the scene transition fade effect.
     */
    public class SceneFader : MonoBehaviour
    {        
        [SerializeField] private float fadeDuration;                    //!< Actual used fade duration.

        [SerializeField] private Image fadeImage;                       //!< UI image of the fade effect.
        private FadeState fadeState;                                    //!< Actual fade state, used to define if the image needs to fade in or out.        

        [SerializeField] private AnimationCurve fadeCurve;              //!< Curve determining the fade time behavior.  

        // Callback for fade end.
        public delegate void Callback();
        Callback faderCallback;        

        /// Routine to executes the fade loop.
        private IEnumerator FadeLoop ()
        {
            float fadeTimer = 0f;
            float fadeProportion = 0f;
            WaitForEndOfFrame delay = new WaitForEndOfFrame();

            while (fadeProportion < 1f)
            {
                fadeTimer += Time.deltaTime;
                fadeProportion = fadeTimer / fadeDuration;

                FadeTransition(fadeProportion);
                yield return delay;
            }

            FadeTransition(1f);

            if (fadeState == FadeState.FadeOut)
            {
                fadeImage.enabled = false;
            }

            if (faderCallback != null)
            {
                faderCallback();
            }            
        }

        /// Interpolates the color of the image to fade it in or out.
        private void FadeTransition (float fadeProportion)
        {
            float fadeValue = 0f;

            if (fadeState == FadeState.FadeOut)
            {
                fadeValue = Mathf.Lerp(1f, 0f, fadeCurve.Evaluate(fadeProportion));
            }
            else if (fadeState == FadeState.FadeIn)
            {
                fadeValue = Mathf.Lerp(0f, 1f, fadeCurve.Evaluate(fadeProportion));
            }

            Color actualColor = fadeImage.color;
            actualColor.a = fadeValue;
            fadeImage.color = actualColor;            
        }

        /// <summary>
        /// Starts to fade the scene accordingly to the option selected (in or out).
        /// </summary>
        public void FadeScene (FadeState fadeOption, Callback callback = null)
        {
            fadeState = fadeOption;            
            faderCallback = callback;
            fadeImage.enabled = true;            

            StartCoroutine(FadeLoop());
        }        
    }
}
