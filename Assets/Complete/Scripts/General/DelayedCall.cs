using System.Collections;
using UnityEngine;

namespace SATI19Complete
{
    /*! \class DelayedCall
     *  \brief Used to call a method after a delay time.
     */
    public class DelayedCall
    {
        public delegate void SimpleCallback();                              //!< A void callback without parameters.  
        public delegate void PositionCallback(Vector3 point);               //!< A void callback with a position parameter.
                
        /// Routine that manages the delayed method call.
        private static IEnumerator DelayRotine (float delayTime, SimpleCallback callback)
        {
            WaitForSeconds delay = new WaitForSeconds(delayTime);

            yield return delay;

            callback();
        }        

        /// <summary>
        /// Invokes a method after a delay time.
        /// </summary>        
        public static void Invoke (SimpleCallback callback, float delayTime, MonoBehaviour runner)
        {
            runner.StartCoroutine(DelayRotine(delayTime, callback));
        }

        /// Routine that manages the delayed method call.
        private static IEnumerator DelayRotine(float delayTime, Vector3 point, PositionCallback callback)
        {
            WaitForSeconds delay = new WaitForSeconds(delayTime);

            yield return delay;

            callback(point);
        }

        /// <summary>
        /// Invokes a method after a delay time.
        /// </summary>        
        public static void Invoke (PositionCallback callback, Vector3 point, float delayTime, MonoBehaviour runner)
        {
            runner.StartCoroutine(DelayRotine(delayTime, point, callback));
        }
    }
}
