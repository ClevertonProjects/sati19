﻿namespace SATI19Complete
{
    /*! \class Tags
     *  \brief Has all the games tags.
     */
    public class Tags
    {
        public const string Player = "Player";                              //!< Player character tag.
        public const string Teleporter = "Teleporter";                      //!< Teleporter item tag.
        public const string InitialCamera = "InitialCamera";                //!< Tag of the level beginning camera.
        public const string CameraController = "CameraController";          //!< Camera transition controller.
        public const string GameController = "GameController";              //!< Game controller.
        public const string SceneFader = "SceneFader";                      //!< Game scene fader controller.
        public const string LevelBeginPoint = "LevelBeginPoint";            //!< Level beginning position.
    }
}
