﻿namespace SATI19Complete
{
    /*! PanelRequirements enum, Lists the conditions of the trigger panels.*/
    public enum PanelRequirements
    {
        BlueIdol,
        YellowIdol,
        None,
    }
}
