namespace SATI19Complete
{
    /*! FadeState enum, Lists the scene fade states. */  
    public enum FadeState
    {
        FadeIn,
        FadeOut,
    }
}
