﻿
namespace SATI19Complete
{
    /*! \interface IInteractables
     *  \brief Added to objects that can interact with the player.
     */
    public interface IInteractables
    {
        void Interact();
    }
}

