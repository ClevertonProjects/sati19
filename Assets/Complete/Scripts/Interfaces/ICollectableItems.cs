﻿
namespace SATI19Complete
{
    /*! \interface ICollectableItems
     *  \brief Added on items that can be grabbed by the player.
     */
    public interface ICollectableItems
    {
        void OnCollect();
    }
}
