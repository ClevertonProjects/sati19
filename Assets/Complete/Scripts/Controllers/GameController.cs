﻿using UnityEngine;
using UnityEngine.Events;

namespace SATI19Complete
{
    /*! \class GameController
     *  \brief Manages the game events.
     */
    public class GameController : MonoBehaviour
    {
        public GameObject SceneFaderPrefab;                                         //!< Scene fader object.        
        public UnityEvent OnGameOver;                                               //!< Event called on game over.
        public UnityEvent OnGameComplete;                                           //!< Event called on game complete.

        private bool isGameOver;                                                    //!< Check when the game is over.
        private bool[] levelRequirements;                                           //!< Track the level requirements progress.
        private const int AmountOfRequirements = 2;                                 //!< Number of requirements in the level.

        public bool IsGameOver
        {
            get { return isGameOver; }
        }


        // Start is called before the first frame update
        void Start()
        {
            levelRequirements = new bool[AmountOfRequirements];
            CreateSceneFader();
        }

        /// Creates the game scene fader.
        private void CreateSceneFader ()
        {
            GameObject fader = Instantiate(SceneFaderPrefab);
            fader.GetComponent<SceneFader>().FadeScene(FadeState.FadeOut);
        }

        /// <summary>
        /// Check if a level requirement was collected.
        /// </summary>
        public bool IsRequirementAchieved (int requirementIndex)
        {
            return levelRequirements[requirementIndex];
        }

        /// <summary>
        /// Sets an item as collected.
        /// </summary>        
        public void CollectItem (int requirementIndex)
        {
            levelRequirements[requirementIndex] = true;
        }

        /// <summary>
        /// Triggers the game over event.
        /// </summary>
        public void TriggerGameOver ()
        {
            isGameOver = true;
            OnGameOver.Invoke();
        }

        /// <summary>
        /// Triggers the game complete event.
        /// </summary>
        public void LevelComplete()
        {
            OnGameComplete.Invoke();
        }

        /// Resets the player to the initial position.        
        private void ResetLevel ()
        {
            Vector3 resetPosition = GameObject.FindGameObjectWithTag(Tags.LevelBeginPoint).transform.position;
            PlayerController playerController = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
            playerController.ReviveAt(resetPosition);

            SceneFader fader = GameObject.FindGameObjectWithTag(Tags.SceneFader).GetComponent<SceneFader>();
            fader.FadeScene(FadeState.FadeOut);
            isGameOver = false;
        }

        /// <summary>
        /// Event called when the retry button is clicked.
        /// </summary>
        public void OnRetryClicked ()
        {
            SceneFader fader = GameObject.FindGameObjectWithTag(Tags.SceneFader).GetComponent<SceneFader>();
            fader.FadeScene(FadeState.FadeIn, ResetLevel);
        }
    }
}
