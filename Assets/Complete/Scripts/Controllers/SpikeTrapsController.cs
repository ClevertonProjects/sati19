﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace SATI19Complete
{
    /*! \class SpikeTrapsController
     *  \brief Controls the synchronization of a group of spike traps.
     */
    public class SpikeTrapsController : MonoBehaviour
    {
        public UnityEvent[] ActivateEvent;                          //!< Spikes' movement activation event.
        public float[] StartingDelays;                              //!< Traps starting movement delay.

        private int actualGroupIndex;                               //!< Index of the actual trap group active.
        private int trapsMovementComplete;                          //!< Counts the amount of traps that have finished the in/out movement.

        private void Start ()
        {
            StartCoroutine(ActivationLoop());
        }

        private IEnumerator ActivationLoop ()
        {
            for (int i = 0; i < StartingDelays.Length; i++)
            {
                yield return new WaitForSeconds(StartingDelays[actualGroupIndex]);
                ActivateEvent[actualGroupIndex].Invoke();

                actualGroupIndex++;
            }
        }

        /// <summary>
        /// Called when a trap movement ended.
        /// </summary>
        public void MovementEnded ()
        {
            trapsMovementComplete++;

            if (trapsMovementComplete >= StartingDelays.Length)
            {
                actualGroupIndex = 0;
                trapsMovementComplete = 0;
                StartCoroutine(ActivationLoop());
            }
        }
    }
}
