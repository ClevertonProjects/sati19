﻿using UnityEngine;

namespace SATI19Complete
{
    /*! \class CameraController
     *  \brief Controls the vitual cameras activation.
     */
    public class CameraController : MonoBehaviour
    {
        private GameObject actualCamera;                                //!< Actual activated game camera.

        private void Start ()
        {
            actualCamera = GameObject.FindGameObjectWithTag(Tags.InitialCamera);
        }

        /// <summary>
        /// Activates a new camera.
        /// </summary>        
        public void CutToCamera (GameObject newCamera)
        {
            actualCamera.SetActive(false);
            newCamera.SetActive(true);

            actualCamera = newCamera;
        }
    }
}
