﻿using UnityEngine;

namespace SATI19Complete
{
    /*! \class PlayerAnimation
     *  \brief Controls the character animations.
     */
    public class PlayerAnimation : MonoBehaviour
    {
        private Animator anim;                                                  //!< Animator component.
            
        private readonly int hashMove = Animator.StringToHash("Move");          //!< Hash value of animator's move parameter.
        private readonly int hashJump = Animator.StringToHash("Jump");          //!< Hash value of animator's jump parameter.
        private readonly int hashGrab = Animator.StringToHash("Grab");          //!< Hash value of animator's grab parameter.
        private readonly int hashThrow = Animator.StringToHash("Throw");        //!< Hash value of animator's throw parameter.
        private readonly int hashInteract = Animator.StringToHash("Interact");  //!< Hash value of animator's interact parameter.
        private readonly int hashDeath = Animator.StringToHash("Death");        //!< Hash value of animator's death parameter.

        private readonly int hashJumpState = Animator.StringToHash("JumpState");    //!< Animator's jump state.
        private readonly int hashIdleState = Animator.StringToHash("Idle");         //!< Animator's idle state.

        public const float GrabActDelay = 0.74f;                                //!< Delay time to process the grab action.
        public const float GrabDuration = 1.05f;                                //!< Grab animation duration.
        public const float ThrowActDelay = 0.7f;                                //!< Delay time to process the throw action.
        public const float ThrowDuration = 2.9f;                                //!< Throwing animation duration.
        public const float InteractActDelay = 0.54f;                            //!< Delay time to process the grab action.
        public const float InteractDuration = 1.2f;                             //!< Grab animation duration.        

        // Start is called before the first frame update
        void Start()
        {
            anim = GetComponent<Animator>();
        }

        /// <summary>
        /// Triggers the movement animation.
        /// </summary>
        public void TriggerMoveAnimation ()
        {
            anim.SetBool(hashMove, true);
        }

        /// <summary>
        /// Stops the movement animation.
        /// </summary>
        public void StopMoveAnimation ()
        {
            anim.SetBool(hashMove, false);
        }

        /// <summary>
        /// Triggers the jump animation.
        /// </summary>
        public void TriggerJumpAnimation ()
        {            
            anim.SetBool(hashJump, true);
        }

        /// <summary>
        /// Restarts the jump animation.
        /// </summary>
        public void RestartJumpAnimation ()
        {
            anim.Play(hashJumpState, 0, 0);
        }

        /// <summary>
        /// Sets the jump animation to end.
        /// </summary>
        public void SetJumpEnd ()
        {
            anim.SetBool(hashJump, false);
        }

        /// <summary>
        /// Triggers the grab animation.
        /// </summary>
        public void TriggerGrabAnimation()
        {
            anim.SetTrigger(hashGrab);
        }

        /// <summary>
        /// Triggers the throw animation.
        /// </summary>
        public void TriggerThrowAnimation()
        {
            anim.SetTrigger(hashThrow);
        }

        /// <summary>
        /// Triggers the interact animation.
        /// </summary>
        public void TriggerInteractAnimation()
        {
            anim.SetTrigger(hashInteract);
        }

        /// <summary>
        /// Triggers the death animation.
        /// </summary>
        public void TriggerDeathAnimation ()
        {
            anim.SetTrigger(hashDeath);
        }

        /// <summary>
        /// Resets to idle state, after death.
        /// </summary>
        public void ResetToIdle ()
        {
            anim.Play(hashIdleState, 0, 0);
        }
    }
}