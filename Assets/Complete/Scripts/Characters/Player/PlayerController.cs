﻿using UnityEngine;

namespace SATI19Complete
{
    /*! \class PlayerController
     *  \brief Controls the player's character movement.
     */
    public class PlayerController : MonoBehaviour
    {
        public float MoveSpeed;                                             //!< Character's moving speed.
        public float JumpSpeed;                                             //!< Character's jumping speed.
        public float MaxFallingSpeed;                                       //!< Maximum falling speed.
        public float Gravity;                                               //!< Custom gravity value, used to speed up the character's falling speed.        

        public Transform charMeshTransform;                                 //!< Character's mesh transform, used to flip the character on move.
        public Transform throwPoint;                                        //!< Point where the throwing item will spawn.        

        public LayerMask CollisionLayer;                                    //!< Layer mask used to check for collisions.
        public LayerMask CollectablesLayer;                                 //!< Layer mask of collectable items.
        public LayerMask InteractableLayer;                                 //!< Layer mask of interactable objects.
        private float collisionCheckDelay;                                  //!< Counts a frame delay to start to check ground collision again.

        public GameObject[] DoubleJumpBoots;                                //!< Meshes activated when the double jump is unlocked.

        public AudioSource JumpBeginSound;                                  //!< Sound effect played on jump begin.
        public AudioSource JumpEndSound;                                    //!< Sound effect played on jump end.
        public AudioSource StepsSound;                                      //!< Sound effect played on each character's step.

        private bool triggerJump;                                           //!< Check if the jump is triggered.
        private bool isGrounded;                                            //!< Check if the character is on ground.
        private bool isMoving;                                              //!< Check if the character is moving.
        private bool isFacingRight = true;                                  //!< Check if the character's mesh is facing the right direction.

        private bool hasTeleport;                                           //!< Check if the character has the teleport device.
        private bool isTeleportActive;                                      //!< Check if the teleport is active to use.
        private bool hasDoubleJump;                                         //!< Check if the double jump hability is unlocked.
        private bool isDoubleJumpActive;                                    //!< Check if the double jump hability was used.
        private bool isThrowing;                                            //!< Check if the character is throwing an object.
        private bool isGrabbing;                                            //!< Check if the character is grabbing an object.
        private bool isInteracting;                                         //!< Check if the character is interacting with a scene object.
        private bool lockInputs;                                            //!< Set to lock the character's inputs verification.

        private PlayerAnimation playerAnimation;                            //!< Character's animation controller.
        private Teleporter teleporter;                                      //!< Teleporter item.
        private Collider[] groundResult;                                    //!< Cached ground check result.
        private RaycastHit[] wallCheckResult;                               //!< Cached wall check result.
        private Rigidbody rb;                                               //!< Character's Rigidbody component.

        private const int GroundCheckFramesDelay = 10;                      //!< Amount of frames to wait before start checking ground collision.        
        private const float WallCheckDistance = 0.35f;                      //!< Distance, ahead of the character, to check if he is close to a wall.
        private const float GrabCheckOffset = 0.2f;                         //!< Distance, from the character's center, to set the grab raycast position.
        private const float InteractCheckOffset = 0.3f;                     //!< Distance, from the character's center, to set the interact raycast position.

        private const string MoveAxisInputName = "Horizontal";              //!< Input name of the horizontal moving axis.
        private const string JumpInputName = "Jump";                        //!< Input name of the jump.
        private const string ThrowInputName = "Throw";                      //!< Input name of throw action.
        private const string GrabInputName = "Grab";                        //!< Input name of grab action.
        private const string InteractInputName = "Interact";                //!< Input name of interact action.

        private void Start()
        {
            playerAnimation = GetComponent<PlayerAnimation>();
            teleporter = GameObject.FindGameObjectWithTag(Tags.Teleporter).GetComponent<Teleporter>();
            groundResult = new Collider[1];
            wallCheckResult = new RaycastHit[1];
            rb = GetComponent<Rigidbody>();
        }

        /// Check if the character is blocked to receive any new input.
        private bool IsLocked ()
        {
            return isGrabbing || isThrowing || isInteracting || lockInputs;
        }

        /// Flips the character mesh.
        private void Flip (float direction)
        {
            if ((direction < 0 && isFacingRight) || (direction > 0 && !isFacingRight))
            {
                isFacingRight = !isFacingRight;

                Vector3 charRotation = charMeshTransform.eulerAngles;
                charRotation.y *= -1f;
                charMeshTransform.eulerAngles = charRotation;
            }            
        }

        /// Sets the character movement animation.
        private void SetMovementAnimation (float h)
        {
            if (h != 0 && !isMoving)
            {
                isMoving = true;
                playerAnimation.TriggerMoveAnimation();
            }
            else if(Mathf.RoundToInt(h) == 0 && isMoving)
            {
                isMoving = false;
                playerAnimation.StopMoveAnimation();
            }
        }

        /// Check if the character is by a wall.
        private bool IsByAWall (float h)
        {
            float upwardOffset = 0.5f;
            Vector3 origin = rb.position + Vector3.up * upwardOffset;
            Vector3 direction = Vector3.right * h;

            //Debug.DrawLine(origin, origin + direction * WallCheckDistance, Color.green);

            if (Physics.RaycastNonAlloc(origin, direction, wallCheckResult, WallCheckDistance, CollisionLayer) > 0)
            {
                return true;
            }

            return false;
        }

        /// Move the character.
        private void Move()
        {
            if (IsLocked()) return;

            float h = Input.GetAxisRaw(MoveAxisInputName);                    
            Vector3 deltaPosition = Vector3.right * h * MoveSpeed * Time.deltaTime;            

            Flip(h);

            if (h != 0f && IsByAWall(h))
            {                
                h = 0f;
                deltaPosition = Vector3.zero;
            }            

            SetMovementAnimation(h);

            rb.position += deltaPosition;
        }

        /// Apply the gravity force in the character.
        private void ApplyGravity()
        {
            float actualFallSpeed = rb.velocity.y - Gravity * Time.deltaTime;

            actualFallSpeed = Mathf.Max(actualFallSpeed, MaxFallingSpeed);
            rb.velocity = new Vector3(rb.velocity.x, actualFallSpeed, rb.velocity.z);
        }

        /// Controls the character jump.
        private void Jump()
        {
            if (triggerJump)
            {
                JumpBeginSound.Play();
                playerAnimation.TriggerJumpAnimation();
                triggerJump = false;
                rb.AddForce(Vector3.up * JumpSpeed, ForceMode.Impulse);
            }

            if (!isGrounded)
            {
                ApplyGravity();
            }
        }

        /// Check if the character is grounded.
        private void CheckGroundCollision()
        {
            Vector3 halfBoxDim = new Vector3(0.125f, 0.025f, 0.125f);

            if (collisionCheckDelay <= 0)
            {
                if (Physics.OverlapBoxNonAlloc(rb.position, halfBoxDim, groundResult, Quaternion.identity, CollisionLayer) > 0)
                {
                    if (!isGrounded)
                    {
                        playerAnimation.SetJumpEnd();
                        JumpEndSound.Play();

                        if (hasDoubleJump)
                        {
                            isDoubleJumpActive = true;
                        }
                    }

                    isGrounded = true;
                }
                else
                {
                    isGrounded = false;
                }
            }
            else
            {
                isGrounded = false;

                if (collisionCheckDelay > 0)
                {
                    collisionCheckDelay--;
                }
            }
        }

        /// Process the grab action.
        private void ProcessGrab ()
        {
            Vector3 halfBoxDim = new Vector3(0.25f, 0.5f, 0.25f);
            int dir = isFacingRight ? 1 : -1;
            Vector3 grabCheckPosition = rb.position + Vector3.right * dir * GrabCheckOffset;
            Collider[] grabbedColliders = Physics.OverlapBox(grabCheckPosition, halfBoxDim, Quaternion.identity, CollectablesLayer);
            
            if (grabbedColliders != null && grabbedColliders.Length > 0)
            {
                for (int i = 0; i < grabbedColliders.Length; i++)
                {
                    grabbedColliders[i].GetComponent<ICollectableItems>().OnCollect();

                    if (grabbedColliders[i].CompareTag(Tags.Teleporter))
                    {
                        hasTeleport = true;
                        isTeleportActive = false;
                    }
                }
            }           
        }

        /// Action called when the grab animation ends.
        private void OnGrabbingEnd ()
        {
            isGrabbing = false;            
        }
        
        /// Process the throw action.
        private void ProcessThrow ()
        {            
            teleporter.Throw(throwPoint.position, throwPoint.rotation);
        }

        /// Action called when the throwing animation ends.
        private void OnThrowingEnd ()
        {
            isThrowing = false;
        }

        /// Process the interact action.
        private void ProcessInteract ()
        {
            Vector3 halfBoxDim = new Vector3(0.25f, 0.5f, 0.25f);
            int dir = isFacingRight ? 1 : -1;
            Vector3 interactCheckPosition = rb.position + new Vector3(1f * dir, 1f, 0f) * InteractCheckOffset;
            Collider[] interactedColliders = Physics.OverlapBox(interactCheckPosition, halfBoxDim, Quaternion.identity, InteractableLayer);

            if (interactedColliders != null && interactedColliders.Length > 0)
            {
                for (int i = 0; i < interactedColliders.Length; i++)
                {
                    interactedColliders[i].GetComponent<IInteractables>().Interact();                    
                }
            }
        }

        /// Action called when the interact animation ends.
        private void OnInteractEnd()
        {
            isInteracting = false;
        }

        /// Gets the player inputs.
        private void GetInputs ()
        {
            if (IsLocked()) return;

            if (isGrounded)
            {
                if (Input.GetButtonDown(ThrowInputName))
                {
                    if (hasTeleport && !isTeleportActive)
                    {
                        SetMovementAnimation(0f);
                        isThrowing = true;                        
                        isTeleportActive = true;
                        playerAnimation.TriggerThrowAnimation();

                        DelayedCall.Invoke(ProcessThrow, PlayerAnimation.ThrowActDelay, this);
                        DelayedCall.Invoke(OnThrowingEnd, PlayerAnimation.ThrowDuration, this);
                    }
                    else if (isTeleportActive && teleporter.IsReadyToUse)
                    {                        
                        rb.position = teleporter.GetTeleporterPosition();                        
                    }
                }
                else if (Input.GetButtonDown(GrabInputName))
                {
                    SetMovementAnimation(0f);
                    isGrabbing = true;
                    playerAnimation.TriggerGrabAnimation();

                    DelayedCall.Invoke(ProcessGrab, PlayerAnimation.GrabActDelay, this);
                    DelayedCall.Invoke(OnGrabbingEnd, PlayerAnimation.GrabDuration, this);
                }
                else if (Input.GetButtonDown(InteractInputName))
                {
                    SetMovementAnimation(0f);
                    isInteracting = true;
                    playerAnimation.TriggerInteractAnimation();

                    DelayedCall.Invoke(ProcessInteract, PlayerAnimation.InteractActDelay, this);
                    DelayedCall.Invoke(OnInteractEnd, PlayerAnimation.InteractDuration, this);
                }
                else if (Input.GetButtonDown(JumpInputName))
                {
                    collisionCheckDelay = GroundCheckFramesDelay;
                    isGrounded = false;
                    triggerJump = true;
                }
            }         
            else if (hasDoubleJump && isDoubleJumpActive)
            {
                if (Input.GetButtonDown(JumpInputName))
                {
                    rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
                    isDoubleJumpActive = false;
                    collisionCheckDelay = GroundCheckFramesDelay;                    
                    triggerJump = true;

                    playerAnimation.RestartJumpAnimation();
                }
            }
        }

        private void Update()
        {
            CheckGroundCollision();
            GetInputs();            
        }

        private void FixedUpdate()
        {
            Move();
            Jump();
        }

        //void OnDrawGizmos()
        //{            
        //    //Check that it is being run in Play Mode, so it doesn't try to draw this in Editor mode
        //    if (rb != null)
        //    {
        //        Gizmos.color = Color.red;
        //        Vector3 boxDim = new Vector3(0.25f, 0.05f, 0.25f);
        //        Vector3 grabBoxDim = new Vector3(0.5f, 0.05f, 0.5f);
        //        Vector3 interactBoxDim = new Vector3(0.5f, 1.0f, 0.5f);
        //        int dir = isFacingRight ? 1 : -1;

        //        //Draw a cube where the OverlapBox is (positioned where your GameObject is as well as a size)
        //        Gizmos.DrawWireCube(rb.position, boxDim);

        //        Gizmos.color = Color.green;
        //        Gizmos.DrawWireCube(rb.position + Vector3.right * dir * 0.2f, grabBoxDim);

        //        Gizmos.color = Color.cyan;
        //        Gizmos.DrawWireCube(rb.position + new Vector3(1f * dir, 1f, 0f) * 0.3f, interactBoxDim);
        //    }
        //}
                
        /// Sets the character in a specific position.        
        private void SendToPosition (Vector3 toPosition)
        {
            rb.position = toPosition;
            lockInputs = false;
        }

        /// <summary>
        /// Triggers the character relocation teleport.
        /// </summary>        
        public void TriggerRelocation (Vector3 toPosition, float relocationDuration)
        {
            lockInputs = true;
            DelayedCall.Invoke(SendToPosition, toPosition, relocationDuration, this);
        }
        
        /// <summary>
        /// Unlocks the double jump hability.
        /// </summary>
        public void UnlockDoubleJump ()
        {
            hasDoubleJump = true;
            isDoubleJumpActive = true;

            DoubleJumpBoots[0].SetActive(true);
            DoubleJumpBoots[1].SetActive(true);
        }

        /// <summary>
        /// Triggers the character death state.
        /// </summary>
        public void Die ()
        {
            lockInputs = true;
            playerAnimation.TriggerDeathAnimation();

            if (isTeleportActive)
            {
                isTeleportActive = false;
                Teleporter teleporter = GameObject.FindGameObjectWithTag(Tags.Teleporter).GetComponent<Teleporter>();
                teleporter.OnCollect();
            }
        }

        /// <summary>
        /// Revives the character at a specific position.
        /// </summary>        
        public void ReviveAt (Vector3 position)
        {
            playerAnimation.ResetToIdle();
            SendToPosition(position);
        }

        /// <summary>
        /// Event called from the run animation to play the footsteps sound.
        /// </summary>
        public void PlayStepSound ()
        {
            if (isGrounded)
            {
                StepsSound.Play();
            }
        }

        /// <summary>
        /// Event called when the game ends.
        /// </summary>
        public void EndGame ()
        {
            lockInputs = true;
            playerAnimation.StopMoveAnimation();
        }
    }    
}