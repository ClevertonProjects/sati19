﻿using UnityEngine;

namespace SATI19Complete
{
    /*! \class DamageTrigger
     *  \brief Applies damage to the triggering object.
     */
    public class DamageTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Tags.Player))
            {
                GameController gameController = GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<GameController>();

                if (!gameController.IsGameOver)
                {
                    other.GetComponent<PlayerController>().Die();
                    gameController.TriggerGameOver();
                }
            }
        }
    }
}
