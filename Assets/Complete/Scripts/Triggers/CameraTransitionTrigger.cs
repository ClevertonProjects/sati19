﻿using UnityEngine;

namespace SATI19Complete
{
    /*! \class CameraTransitionTrigger
     *  \brief Triggers the transition to a new camera.
     */
    public class CameraTransitionTrigger : MonoBehaviour
    {
        [SerializeField] private GameObject cameraToTrigger;                        //!< Camera to activate.

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Tags.Player))
            {
                CameraController cameraController = GameObject.FindGameObjectWithTag(Tags.CameraController).GetComponent<CameraController>();
                cameraController.CutToCamera(cameraToTrigger);
            }
        }
    }
}
