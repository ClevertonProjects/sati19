﻿using UnityEngine;

namespace SATI19Complete
{
    /*! \class ExitTrigger
     *  \brief Mark the level end.
     */
    public class ExitTrigger : MonoBehaviour
    {
        private void OnTriggerEnter (Collider other)
        {
            if (other.CompareTag(Tags.Player))
            {
                other.GetComponent<PlayerController>().EndGame();
                GameController gameController = GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<GameController>();
                gameController.LevelComplete();
            }
        }
    }
}